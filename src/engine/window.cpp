#include "engine/app.hpp"

#include <stdexcept>

namespace engine {

BaseWindow::BaseWindow(int w, int h, std::string name) : width{w}, height{h}, windowName{name} {
  initWindow();
}

BaseWindow::~BaseWindow() {
  glfwDestroyWindow(window);
  glfwTerminate();
}

void BaseWindow::initWindow() {
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

  window = glfwCreateWindow(width, height, windowName.c_str(), nullptr, nullptr);
}

  void BaseWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR *surface) {
    if (glfwCreateWindowSurface(instance, window, nullptr, surface) != VK_SUCCESS)
      throw std::runtime_error("failed to create surface");
  }

  void BaseWindow::getFrameBufferSize(int &width, int &height) {
    return glfwGetFramebufferSize(window, &width, &height);
  }
} 