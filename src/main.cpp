#include "main.hpp"
#include "engine/app.hpp"

int main() {
    engine::App app{};

    try
    {
        app.run();
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }

    return 0;
}
