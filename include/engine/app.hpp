#pragma once

#include "engine/window.hpp"
#include "engine/pipeline.hpp"
#include "engine/device.hpp"
#include "engine/swapchain.hpp"
#include "engine/model.hpp"

#include <stdexcept>
#include <memory>
#include <array>
#include <vector>

namespace engine
{
    class App
    {
    public:
        App();
        ~App() { vkDestroyPipelineLayout(device.device(), pipelineLayout, nullptr); }
        static constexpr int WIDTH = 800;
        static constexpr int HEIGHT = 600;

        void run();

    private:
        void loadModels();
        void createPipelineLayout();
        void createPipeline();
        void createCommandBuffers();
        void drawFrame();

        BaseWindow window{WIDTH, HEIGHT, "Hello Vulkan!"};
        Device device{window};
        SwapChain swapchain{device, window.getExtent()};
        std::unique_ptr<Pipeline> pipeline;
        VkPipelineLayout pipelineLayout;
        std::vector<VkCommandBuffer> commandBuffers;
        std::unique_ptr<Model> model;
    };
}